browser.runtime.onMessage.addListener(function(payload) {
  [sessionId, session] = Object.entries(payload)[0];
  if (session.sessionIds) {
    fillLeftColumn(session.sessionIds);
  }
  fillRightColumn(sessionId, session);
});

function fillLeftColumn(sessionIds) {
  for (let sessionId of sessionIds.sort().reverse()) {
    let label = $("<label>");
    let input = $("<input>").attr("type", "radio").attr("name", "session");
    input.attr("value", sessionId);
    span = $("<span>").text(localizeDate(sessionId));
    label.append(input).append(span);
    $("#sessions").append(label);
  }
  $("[name='session']").click(function() {
    browser.runtime.sendMessage([sessionIds, $(this).val()]).then(
      function(payload) {
        [sessionId, session] = Object.entries(payload)[0];
        fillRightColumn(sessionId, session);
      },
      function(err) {
        alert(err);
      }
    );
  });
}

function fillRightColumn(sessionId, session) {
  $("#session").empty();
  let local = localizeDate(sessionId);
  $("#session-date").text(local);
  for (let tab of session) {
    let label = $("<label>");
    let input = $("<input>").attr("type", "checkbox").attr("name", sessionId);
    input.attr("checked", "checked"); // TODO; make optional
    input.data("url", tab.url);
    input.addClass("checkbox");
    let anchor = $("<a>").attr("href", tab.url);
    let favicon = $("<img>").attr("src", tab.favIconUrl).addClass("favicon");
    let span = $("<span>").text(tab.title);
    span.attr("title", `last accessed ${localizeDate(tab.lastAccessed)}`);
    anchor.append(favicon).append(span);
    label.append(input).append(anchor);
    $("#session").append(label);
  }
}

function localizeDate(sessionId) {
  return (new Date(Number(sessionId))).toLocaleString();
}

$("#open-tabs").click(function() {
  $(".checkbox:checked").each(function(index) {
    let url = $(this).data("url");
    let name = index == 1 ? "_self" : "_blank";
    window.open(url, name);
  });
});

true;
