let store = browser.storage.local;


let currentTab;

// console.log("background.js checking in");

store.get("sessions").then(
  function (result) {
    let entries = Object.entries(result);
    if (entries.length == 0) {
      sessionIds = [];
    }
    else {
      [_, sessionIds] = entries[0];
    }
    let currentSessionId = String(Date.now());
    sessionIds.push(currentSessionId);
    store.set({sessions: sessionIds});
    saveSession(currentSessionId);
    browser.tabs.onUpdated.addListener(function (_tabId, _changeInfo, _tab) {
      saveSession(currentSessionId);
    });
    browser.tabs.onRemoved.addListener(function (_tabId, _removeInfo) {
      saveSession(currentSessionId);
    });
    browser.browserAction.onClicked.addListener(tabOpener(sessionIds, currentSessionId));
  },
  function (err) {console.error(err);}
);

function saveSession(sessionId) {
  browser.tabs.query({}).then(
    function (tabs) {
      objToSave = {};
      objToSave[sessionId] = tabs;
      store.set(objToSave);
    },
    function (err) {console.error(err);}
  );
};

function tabOpener(sessionIds, sessionId) {
  browser.runtime.onMessage.addListener(messageListener);
  return function (_tab) {
    browser.tabs.create({url: "tab.html"}).then(
      function (tab) {
        currentTab = tab; // kludge
        store.get(sessionId).then(
          displayInTab(tab, sessionIds),
          function (err) {console.error(err);}
        );
      },
      function (err) {console.error(err);}
    );
  };
}

function displayInTab(tab, sessionIds) {
  return function(payload) {
    [_, session] = Object.entries(payload)[0];
    session.sessionIds = sessionIds;
    browser.tabs.executeScript(tab.id, {file: "jquery.js"}).then(
      function () {
        browser.tabs.executeScript(tab.id, {file: "content.js"}).then(
          function () {
            browser.tabs.sendMessage(tab.id, payload);
          },
          function (err) {console.error(err);}
        );
      },
      function (err) {console.error(err);}
    );
  }
}

// payload is [sessionIds, sessionId]
function messageListener(payload) {
  [sessionIds, sessionId] = payload;
  return store.get(sessionId);
}

